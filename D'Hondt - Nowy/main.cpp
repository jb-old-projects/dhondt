/********************************************************************************************************/
/*						Autor:							Jakub Biskup									*/
/*						Kierunek:						Informatyka										*/
/*						Rok studi�w:					I												*/
/*						Semestr:						I												*/
/*						Data: ostatniej modyfikacji:	7 grudnia 2015, 21:20							*/
/********************************************************************************************************/
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstdlib>

using namespace std;

void exit(int status);

bool DataValidation(string line)
{
	int k = 0;
	for (int i = 0; i < line.length(); i++)
	{
		if (line[i] == '0' || line[i] == '1' || line[i] == '2' || line[i] == '3' || line[i] == '4' || line[i] == '5' || line[i] == '6' || line[i] == '7' || line[i] == '8' || line[i] == '9' || line[i] == ' ')
			k++;
		else
			return false;
	}
	if (k == line.length())
		return true;
}

void str_to_int(string line, vector <int> &data)		// Funkcja ta ma na celu znalezienie i odpowiednie przekonwertowanie podci�g�w liczb ze wczytanej linii
{
	int n = 0;

	for (int i = 0; i < line.length(); i++)
	{
		if (line[i] == ' ')								// sprawdzenie, czy dany podci�g si� ju� sko�czy�
		{
			data.push_back(n);
			n = 0;
			continue;
		}
		n = n * 10 + (int)line[i] - 48;

		if (i == line.length() - 1)						 // sprawdzenie, czy dana wczytana linia zosta�a w ca�o�ci sprawdzona
		{
			data.push_back(n);
			break;
		}
	}

}

void dhondt(int s, vector <int> &v, vector <float> &f, vector <int> &m)		// Jest to funkcja maj�ca na celu obliczenie obecnego wsp�czynnika oraz sprawdzenie, kt�ra partia powinna otrzyma� mandat
{
		int max = -1;
		int max_pos;

		for (int i = 0; i < v.size(); i++)
		{
			f[i] = ((v[i] / (m[i] + 1)));

			if (f[i] > max)
			{
				max = f[i];
				max_pos = i;
			}
		}
		m[max_pos]++;
}

void DataOperation(string input, string output)					// funkcja ta ma za zadanie odpowiednie dzia�anie na plikach - poprawny odczyt oraz zapis
{
	ifstream read;
	read.open(input);

	if (!read.good())
	{
		cout << "Nie udalo sie otworzyc pliku!" << endl;
		system("pause");
	}

	ofstream record(output);

	while (!read.eof())
	{
		vector <int> data;
		vector <int> votes;
		vector <float> factor;
		vector <int> mandates;
		string line;

		getline(read, line);

		if (!DataValidation(line))
		{
			cout << "Podano nieprawidlowy format danych!" << endl;
			system("pause");
			exit(0);
		}

		str_to_int(line, data);

		int seats = data[0];

		for (int i = 0; i < data.size() - 1; i++)
		{
			votes.push_back(data[i + 1]);
			factor.push_back(data[i + 1]);
			mandates.push_back(0);
		}

		for (int i = 0; i < seats; i++)
			dhondt(seats, votes, factor, mandates);

		for (int i = 0; i < data.size() - 1; i++)
			record << mandates[i] << " ";

		record << endl;
	}
}

void FileOperation(int argc, char**argv)		// Funkcja ta ma za zadanie umo�liwi� odpowiednie uruchomienie programu z odpowiednimi paramatrami startowymi
{
	int i = 1;
	string input, output;
	
	if (argc == 1)								// sprawdzenie czy plik zosta� odpalony bez parametr�w startowych
		{
				cout << "Prawidlowe parametry startowe programu powinny wygl�dac w nastepujacy sposob: " << endl;
				cout << "./nazwa_programu.exe -i input.txt -o output.txt , gdzie: " << endl;
				cout << "po przelaczniku -i podawana jest nazwa pliku wejsciowego, " << endl;
				cout << "po przelaczniku -o podawana jest nazwa pliku wyjsciowego. " << endl;
				system("pause");
				exit(0);
		}

	while (i < argc)
	{
		string parametr = argv[i];

		if (parametr != "-i" && parametr != "-o" && parametr != "-h")
			cout << "Nieprawidlowe parametry!" << endl;

		if (parametr == "-i")
		{
			i++;
			if (i < argc)
				input = argv[i];
			else
				cout << "Blad w podowaniu nazwy pliku wejsciowego." << endl;
		}

		else if (parametr == "-o")
		{
			i++;
			if (i < argc)
				output = argv[i];
			else
				cout << "Blad w podowaniu nazwy pliku wyjsciowego." << endl;
		}

		else if (parametr == "-h")
		{
			cout << "Prawidlowe parametry startowe programu powinny wygl�dac w nastepujacy sposob: " << endl;
			cout << "./nazwa_programu.exe -i input.txt -o output.txt , gdzie: " << endl;
			cout << "po przelaczniku -i podawana jest nazwa pliku wejsciowego, " << endl;
			cout << "po przelaczniku -o podawana jest nazwa pliku wyjsciowego, " << endl;
			system("pause");
			exit(0);
		}
		i++;
	}

	DataOperation(input, output);
}

int main(int argc, char**argv)
{
	FileOperation(argc, argv);
	system("pause");
	exit(0);
}
